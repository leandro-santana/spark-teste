import findspark
from pyspark.ml.feature import StandardScaler
from pyspark.ml.linalg import DenseVector
from pyspark.ml.regression import LinearRegression
from pyspark.sql import Row
from pyspark.sql import SparkSession
from pyspark.sql.types import FloatType

from utils import convert_col

findspark.init("/usr/local/spark/")

spark = SparkSession.builder \
    .master("local") \
    .appName("SparkTest") \
    .config("spark.executor.memory", "2gb") \
    .getOrCreate()

sc = spark.sparkContext
rdd = sc.textFile('files/Salary_Data.csv')
rdd = rdd.map(lambda line: line.split(","))
df = rdd.map(lambda line: Row(YearsExperience=line[0], Salary=line[1])).toDF()

columns = ['YearsExperience', 'Salary']
df = convert_col(df, columns, FloatType())

# print(df.select('Salary').show(10))
# print(df.groupBy("Salary").count().sort("Salary",ascending=False).show())
# print(df.describe().show())

input_data = df.rdd.map(lambda x: (x[0], DenseVector(x[1:])))
df = spark.createDataFrame(input_data, ["label", "features"])

standardScaler = StandardScaler(inputCol="features", outputCol="features_scaled")
scaler = standardScaler.fit(df)
scaled_df = scaler.transform(df)
# print(scaled_df.take(2))

train_data, test_data = scaled_df.randomSplit([.75, .25], seed=1234)
lr = LinearRegression(labelCol="label", maxIter=10)
linearModel = lr.fit(train_data)
predicted = linearModel.transform(test_data)
print(linearModel.coefficients)
print(linearModel.intercept)
print(linearModel.summary.rootMeanSquaredError)
print(linearModel.summary.r2)

spark.stop()


# from pyspark import SparkConf, SparkContext
# from operator import add
#
# conf = (SparkConf().setMaster("local").setAppName("SPark").set("spark.executor.memory", "2g"))
# sc = SparkContext(conf=conf)
# #
# july = sc.textFile('access_log_Jul95')
# july = july.cache()
#
# august = sc.textFile('access_log_Aug95')
# august = august.cache()
#
# # number of distinct hosts
# july_count = july.flatMap(lambda line: line.split(' ')[0]).distinct().count()
# august_count = august.flatMap(lambda line: line.split(' ')[0]).distinct().count()
#
# print('Distinct hosts on July: %s' % july_count)
# print('Distinct hosts on August %s' % august_count)
#
#
# # number of 404 errors
# def response_code_404(line):
#     try:
#         code = line.split(' ')[-2]
#         if code == '404':
#             return True
#     except Exception as error:
#         pass
#     return False
#
#
# july_404 = july.filter(response_code_404).cache()
# august_404 = august.filter(lambda line: line.split(' ')[-2] == '404').cache()
#
# print('404 errors in July: %s' % july_404.count())
# print('404 errors in August %s' % august_404.count())
#
#
# # 5 most frequent endpoints causing 404 errors
# def top5_endpoints(rdd):
#     endpoints = rdd.map(lambda line: line.split('"')[1].split(' ')[1])
#     counts = endpoints.map(lambda endpoint: (endpoint, 1)).reduceByKey(add)
#     top = counts.sortBy(lambda pair: -pair[1]).take(5)
#
#     print('\nTop 5 most frequent 404 endpoints:')
#     for endpoint, count in top:
#         print(endpoint, count)
#
#     return top
#
#
# top5_endpoints(july_404)
# top5_endpoints(august_404)
#
#
# # 404 errors per day
# def daily_count(rdd):
#     days = rdd.map(lambda line: line.split('[')[1].split(':')[0])
#     counts = days.map(lambda day: (day, 1)).reduceByKey(add).collect()
#
#     print('\n404 errors per day:')
#     for day, count in counts:
#         print(day, count)
#
#     return counts
#
#
# daily_count(july_404)
# daily_count(august_404)
#
#
# # Total byte count
# def accumulated_byte_count(rdd):
#     def byte_count(line):
#         try:
#             count = int(line.split(" ")[-1])
#             if count < 0:
#                 raise ValueError()
#             return count
#         except:
#             return 0
#
#     count = rdd.map(byte_count).reduce(add)
#     return count
#
#
# print('Total byte count in July: %s' % accumulated_byte_count(july))
# print('Total byte count in August: %s' % accumulated_byte_count(august))
#
# sc.stop()
