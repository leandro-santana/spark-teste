from connection_spark import Connection
from find_http_response import FindHttpResponse

connection = Connection(app_name='my_spark_app')
spark_context = connection.spark.sparkContext

file = spark_context.textFile('files/access_log_Jul95').cache()
file_count = file.flatMap(lambda line: line.split(' ')[0]).distinct().count()

print('1 - Total de Hosts únicos: ({total})'.format(total=file_count))
print('----------------------------------------------------------')

file_404 = file.filter(FindHttpResponse.get_404_errors).cache()
print("2 - Total de erros 404: ({total})".format(total=file_404.count()))
print('----------------------------------------------------------')

top5_404 = FindHttpResponse.top5_404(file_404)
print("3 - Os 5 URLs que mais causaram erro 404.")
for host in top5_404:
    print(host)
print('----------------------------------------------------------')

t_404_pear_day = FindHttpResponse.count_daily(file_404)
print("4 - Quantidade de erros 404 por dia.")
for host in t_404_pear_day:
    print(host)
print('----------------------------------------------------------')

print("5 - O total de bytes retornados: ({total}).".format(total=FindHttpResponse.accumulated_byte_count(file)))
