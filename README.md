RDD ->  Resilient Distributed Datasets </br></br>

1 - Qual o objetivo do comando cache em Spark? </br>

R: No Spark temos dois tipos de operações, são elas a operação de transformação e operação de ação,
para cada operação de transformação que pode ser por exemplo map(), filter(), select() e etc o spark cria um ou mais novos RDD, já a operação de ação é que realmente produz um valor ou um resultado visivel como por exemplo um count(), foreach() etc. Nessa caso se tornaria muito ineficiente se sempre que necessário uma operação de transformação+operação de ação a toda momento que se precise dos mesmos resultados já que para cada ação será criado um novo RDD. Nesse caso o comando cache funcionaria como um facilitador armazenando o comando de transformação que previamente são comandos lazy para um futura execução associado a um comando de ação.</br>

2 - O mesmo código implementado em Spark é normalmente mais rápido que a implementação equivalente em MapReduce. Por quê?
Sim, A principio quando se têm diversos jobs mapreduce e que são executados em sequência esses jobs são pontamente armazenados e disco e quando executado o próximo job esse resultado é lido novamente e passado o novo job. Já no Spark é possível ter resultados intermediários passados diretamente por memória utilizando cache o que reduz necessidade de qualquer job ser armazenado em disco e claramente ter um desempenho melhor.</br>

3 - Qual é a função do SparkContext?</br>
R: O SparkContext funciona com um intermediador onde ele aloca a configuração de armazenamento e memória necessário para os workNodes, ele também é um facilitador para conexão de diversos cluster managers como Hadoop por exemplo.</br>

4 - Explique com suas palavras o que é Resilient Distributed Datasets (RDD).</br>
R: O RDD é na verdade o coração do spark é através dele que são armazenados todos os pacotes de ações, ele é Resilient porque torna-se tolerante a falhas, isso porque ele é capaz de recuperar dados perdidos um vez que eles foram distribuidos por diversos nós do cluster através da ação Distributed. Os RDDs são imutavéis, ou seja, uma vez que eles são criados não podem ser mais modificados a única ação a ele é o comando de leitura o que facilita na utilização desses dados para criação de novos RDDs.</br>

5 - GroupByKey é menos eficiente que reduceByKey em grandes dataset. Por quê?</br>
R: ReduceByKey se torna mais eficiênte em um grande conjunto de dados isso porque ele não precisa embaralhar os dados, apenas qualifica-los caso existente. Já o GroupByKey faz um embaralhamento de todos os pares que pode resultar em uma resposta muito maior havendo a possibilidade de um trafego maior em rede e resultando em problema de falta de memória.</br>

6 - Explique o que o código Scala abaixo faz.</br>
```
val textFile = sc.textFile("hdfs://...")
val counts = textFile.flatMap(line => line.split("")).map(word => (word, 1)).reduceByKey(_ + _)
counts.saveAsTextFile("hdfs://...")
```
R: A primeira linha é executado o comando de leitura de um arquivo, na segunda linha ao texto extraido é lançado primeiramente a função flatMap fazendo o split(divisão) de cada palavra no arquivo, na sequência a função map cada split é atrelado a 1 indice e logo na função reduceByKey esses valores são agregados por chave, através da operação de soma. no final o resultado da variavel é salvo em arquivo.</br>

Spark: ```/usr/local/spark/ (version 2.4.0)``` </br>
Python: ```version: 3.7.2```

```
 $ pipenv install

 $ python nasa-spark.py
```

```
1 - Total de Hosts únicos: (47)
```
```
2 - Total de erros 404: (68)
```
```
3 - Os 5 URLs que mais causaram erro 404.
/shuttle/missions/sts-71/images/KSC-95EC-0916.txt : 9
/pub/winvn/readme.txt : 7
/pub/winvn/release.txt : 5
/misc/showcase/personal_edition/images/milan_banner.gif : 5
/history/apollo/publications/sp-350/sp-350.txt~ : 4
```
```
4 - Quantidade de erros 404 por dia.
01/Jul/1995 : 68</br>
```
```
5 - O total de bytes retornados: (279221212).
```

