import base64
import logging
import logging.config


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        hash_cls = generate_hash(str(args) + str(kwargs))
        if hash_cls not in cls._instances:
            cls._instances[hash_cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[hash_cls]

    @staticmethod
    def drop():
        """Drop the instance (for testing purposes)"""
        Singleton._instances = {}


def generate_hash(value):
    """generate hashs"""
    value = value.encode("ascii")
    return base64.b64encode(value).decode('utf8')


def convert_col(df, names, new_type):
    """convert type collumns"""
    for name in names:
        df = df.withColumn(name, df[name].cast(new_type))
    return df


def my_app_log():
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            "simple": {
                "format": "%(asctime)s - [%(levelname)s] - [%(process)d] - [%(filename)s:%(lineno)s] - %(message)s",
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'handlers': {
            "spark": {
                "class": "logging.handlers.TimedRotatingFileHandler",
                "when": 'midnight',
                'formatter': 'simple',
                "filename": "./log_app/log_app_spark.log"
            }
        },
        'loggers': {
            'spark': {
                'handlers': ['spark'],
                'level': 'DEBUG',
                'propagate': False
            }
        }})

    return logging.getLogger('spark')
