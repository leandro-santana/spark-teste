from operator import add

from utils import my_app_log


class FindHttpResponse:
    @staticmethod
    def get_404_errors(line):
        try:
            code = line.split(' ')[-2]
            if code == '404':
                return True
        except Exception as error:
            my_app_log().exception(error)
            pass
        return False

    @staticmethod
    def top5_404(rdd):
        endpoints = rdd.map(lambda line: line.split('"')[1].split(' ')[1])
        counts = endpoints.map(lambda endpoint: (endpoint, 1)).reduceByKey(add)
        top = counts.sortBy(lambda pair: -pair[1]).take(5)

        list_endpoints = []
        for endpoint, count in top:
            list_endpoints.append('{} : {}'.format(endpoint, count))
        return list_endpoints

    @staticmethod
    def count_daily(rdd):
        days = rdd.map(lambda line: line.split('[')[1].split(':')[0])
        counts = days.map(lambda day: (day, 1)).reduceByKey(add).collect()

        list_404_count = []
        for day, count in counts:
            list_404_count.append('{} : {}'.format(day, count))

        return list_404_count

    @staticmethod
    def accumulated_byte_count(rdd):
        def byte_count(line):
            try:
                count = int(line.split(" ")[-1])
                if count < 0:
                    raise ValueError()
                return count
            except Exception as error:
                my_app_log().exception(error)
                return 0

        count = rdd.map(byte_count).reduce(add)
        return count
