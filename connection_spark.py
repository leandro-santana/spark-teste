import findspark
from pyspark.sql import SparkSession

from utils import Singleton


class Connection(metaclass=Singleton):
    def __init__(self, app_name, host='local', memmory='2gb', find_init="/usr/local/spark/"):
        self.__findspark = findspark.init(find_init)
        self.__spark = SparkSession.builder \
            .master(host) \
            .appName(app_name) \
            .config("spark.executor.memory", memmory) \
            .getOrCreate()

    @property
    def spark(self):
        return self.__spark
